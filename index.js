// tic tac toe

// whos go is it?
// victory condition?
// who?
// grid - 3x3
// cells
// filled with what/who?
// where?

// victory condition?
// 0, 1, 2, 3
// 0, 'X', 'O', -1 (the worst of both worlds)
// '', 'X', 'O', '/'

const playStates = {
  playing: "",
  exesWin: "X",
  ohsWin: "O",
  draw: "/"
};

let gameState = {
  victory: playStates.playing,
  dummy: true
};

const changeVictory = (game, to) => ({ ...game, victory: to });

const not = (fn, x) => !fn(x);

const victoryIsPlaying = game => game.victory === playStates.playing;
const victoryIsNotPlaying = game => not(victoryIsPlaying, game);

const winVictory = (game, winner) =>
    victoryIsPlaying(game) ? changeVictory(game, winner) : game;

const resetVictory = game =>
    victoryIsNotPlaying(game)
        ? changeVictory(game, playStates.playing)
        : game;
